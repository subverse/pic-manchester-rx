/////////////////////////////////////////////////////////
//
//Name:			IRRXDATA.C
//
//Author:		David McKelvie <davidmckelvieathotmaildotcom>
//
//Description:	Version of Infrared Communications Interface,
//				decodes a manchester encoded data stream until
//				the receiver buffer is full or a transmission
//				timeout occurs, displays contents of buffer 
//				to LCD display.
//
//Target:		16F627/8/8A
//
//Version:		0.1 12/08/2003
//
//Public Methods:	setup();
//					startRx();
//					bufferGet();
//
//Public Data:		rxFinished
//					fBufferFull
//
//Private Methods:	lcdOpen();
//					bufferInitialise();
//					bufferPut();
//					tmr0Machine();
//					edgeMachine();
//					taskManager();	//interrupt
//
//Private Data:		cirBuffer
//					receiver
//
//Timing:		The basic unit of time used in this program
//				is the half bit time 'T'. To adjust this basic
//				unit use the following formula:
//
//				T = TIME_TO_INT / PRESCALER * (FREQ * 10^6 / 4) uS
//
//				to calculate T for a 300us interrupt using 
//				a PRESCALER of 4, and a 4MHz clock freq
//
//				T = 300*10^-6 / 4 * 4*10^6 / 4
//				T = 75
//
//				TMR0 needs to be loaded with 256 - T to cause
//				a Timer0 overflow interrupt at TIME_TO_INT
//				TMR0 = 256 - 75
//				TMR0 = 181
//
//				To cause an idle time interrupt of 3T
//			
//				3T = T * 3
//				3T = 225
//
//				TMR0 needs to be loaded with 256 - 3T to cause
//				a Timer0 overflow interrupt at 3T
//				TMR0 = 256 - 225
//				TMR0 = 31
//
//				TIME_TO_INT		TMR0_T	TMR0_2T	TMR0_3T
//				100us			231		206		181
//				200us			206		156		106
//				300us pre=4		181		106		31
//				300us pre=8		219		181		106
//				400us			156		56		-
//				500us			131		6		-
//				600us pre=4		106		-		-
//				600us pre=8		181		106		31
//
//
/////////////////////////////////////////////////////////
#include<pic.h>
#include"mypic.h"
#include"lcd.h"
#include"picir.h"
#include"buffer.h"

#define	BUFFER_FULL			1
#define	RX_FINISHED			2
#define	RX_RECEIVING		3
#define	TMR0_T				181
#define	TMR0_3T				31
#define T1_SEED_HIGH		0x06
#define T1_SEED_LOW			0xDC

//__CONFIG( INTIO & UNPROTECT & WDTDIS & LVPDIS );
__CONFIG( XT & UNPROTECT & WDTDIS & LVPDIS );

void setup( void );
void startRx( void );
void interrupt taskManager( void );
void tmr0Machine( void );
void edgeMachine( void );

volatile char tmr0State, edgeState, bitCount, t, onePointFiveT, receivedByte, rxFlag;
bit fBufferFull, rxFinished;

struct queue cirBuffer;

main(void)
{
	char tmp;
	
	setup();
	startRx();
	
	while( TRUE )
	{
		if( rxFinished == TRUE )
		{
			lcdIoctl( LCD_CLEAR_DISPLAY );
			lcdIoctl( LINE_ONE );
			while( bufferGet( &tmp, &cirBuffer ) != FALSE )		
				lcdWrite( tmp );
				
			fBufferFull = FALSE;
			
			startRx();
		}
	}
}

void setup( void )
{
	lcdOpen();
	bufferInitialise( &cirBuffer );
	rxFinished = TRUE;		//disable receiver

//	TRISA   = 0x0f;
	CMCON   = 0x07;			//disable Compare
	CCP1CON = 0;
	//TMR1H   = T1_SEED_HIGH;	//seed timer for 500ms int
	//TMR1L   = T1_SEED_LOW;
	//T1CON  	= 0b00110001;	//tmr1 prescaler, tmr1 on.
	OPTION  = 0b00000001;	//set TMR0 prescaler 1:4
	PEIE    = 1;
	TMR1IE  = 1;
	
/*	//CCP1CON = 0b00001100;
	CCP1CON = 0;			//Capture/Compare/PWM off
	TMR2 = 0;				//Clear Timer2 register
	PEIE = 1;				//Enable peripheral interrupts
	PIE1 = 0;				//Mask all peripheral interrupts except
	TMR2IE = 1;				//the timer 2 interrupts.
	PIR1 = 0;				//Clear peripheral interrupts Flags
	T2CON = 0b01101010;		//Set Postscale = 10, Prescale = 4, Timer 2 = off.
	PR2 = 100;


	TMR2ON = 1;				//Timer2 starts to increment 
*/
	GIE = 1;				//Global interrupt enable.
}

void startRx( void )
{
	tmr0State  = RESET;	//prep TMR0 state
	rxFinished = FALSE;	//clear RX flag
	TMR0       = 200;	//schedule TMR0 int 
	T0IF       = 0;		//clear TMR0 flag
	T0IE       = 1;		//enable TMR0 interrupt
	GIE		   = 1;		//enable interrupts
}

/////////////////////////////////////////////////////////
//
//Name:			taskManager
//
//Description:	Interrupt service routine, coordinates
//				receiver state machine and clock
//
/////////////////////////////////////////////////////////
#pragma interrupt_level 1
void interrupt taskManager( void )
{
	if( rxFinished == FALSE )
	{
		if( T0IF & T0IE )
			tmr0Machine();
		if( INTF & INTE )
			edgeMachine();
	}
}

//#pragma interrupt_level 1
void tmr0Machine( void )
{
	switch(	tmr0State )
	{
		case RESET:					//reset state
		bitCount  = 0;
		edgeState = RESET;			//prep next EDGE state
		T0IE      = 1;				//enable TMR0 overflow interrupt
		INTE      = 1;				//enable EDGE interrupt
		TMR0      = TMR0_3T;		//schedule next TMR0 interrupt
		if( INPUT == 1 )			//if line is not active (active low)
		{
			tmr0State = IDLE;		//prep next TMR0 state
			INTEDG    = 0;			//detect falling EDGE
		}
		else						//if line is active
		{							//schedule callback
			tmr0State = RESET;		//stay in this state
			INTEDG    = 1;			//detect rising EDGE
		}
		break;
		
		case IDLE:					//line is idle (inactive for TMR0 timeout period)
		T0IE      = 0;				//disable TMR0 interrupt
		edgeState = START_BIT_1;	//prep next edge state
		INTE      = 1;				//enable EDGE interrupt
		INTEDG    = 0;				//detect falling EDGE
		break;
		
		case SAMPLE:				//1.5T interrupt, sample line
		receivedByte = receivedByte << 1;	//shift in bit, big endian
		if( INPUT == 0 )
			receivedByte |= 0x01;	//sample line
		tmr0State = DONE;
		T0IE      = 1;				//enable TMR0 interrupt
		TMR0      = 100;			//schedule reset one T
		edgeState = CENTER_EDGE;	//prep next EDGE state
		INTE      = 1;				//enable EDGE interrupt	
		if( INPUT )					//next edge will be...
			INTEDG = 0;				//falling.
		else
			INTEDG = 1;				//rising.
		break;
		
		case DONE:					//TMR0 timeout during receive
		rxFinished = TRUE;			//signal to main
		T0IE = 0;					//disable TMR0 interrupt
		INTE = 0;					//disable EDGE interrupt
		break;
		
		default:					//error
		break;
	}
	T0IF = 0;
	INTF = 0;
}

//#pragma interrupt_level 1
void edgeMachine( void )
{
	switch(	edgeState )
	{

		case RESET:					//reset state
		bitCount  = 0;
		edgeState = RESET;			//prep next EDGE state
		T0IE      = 1;				//enable TMR0 overflow interrupt
		INTE      = 1;				//enable EDGE interrupt
		TMR0      = TMR0_3T;		//schedule next TMR0 interrupt
		if( INPUT == 1 )			//if line is not active (active low)
		{
			tmr0State = IDLE;		//prep next TMR0 state
			INTEDG    = 0;			//detect falling EDGE
		}
		else						//if line is active
		{							//schedule callback
			tmr0State = RESET;		//stay in this state
			INTEDG    = 1;			//detect rising EDGE
		}
		break;
		
		case START_BIT_1:			//start bit first edge
		tmr0State = RESET;			//prep next TMR0 state
		T0IE      = 1;				//enable TMR0 interrupt
		TMR0      = 0;				//schedule TMR0 interrupt 1024us
		edgeState = START_BIT_2;	//prep next EDGE state
		INTE      = 1;				//enable EDGE interrupt
		INTEDG    = 1;				//start bit rising edge
		break;
		
		case START_BIT_2:			//start bit second edge
		//t = 255 - (TMR0 - 20);		//calculate T
		//onePointFiveT = 255 - (t / 2 + t);		//calculate 1.5T
		onePointFiveT = 144; 
		tmr0State = SAMPLE;			//prep next TMR0 state
		T0IE      = 1;				//enable TMR0 interrupt
		TMR0 = onePointFiveT;		//schedule TMR0 interrupt
		INTE      = 0;				//disable EDGE interrupt
		break;
		
		case CENTER_EDGE:			//data bit edge, synchronise
		tmr0State = SAMPLE;			//prep next TMR0 state
		T0IE      = 1;				//enable TMR0 interrupt
		TMR0      = onePointFiveT;	//schedule TMR0 interrupt
		INTE      = 0;				//disable EDGE interrupt
		if( ++bitCount == 8 )		//finished receiving byte?
		{
			bitCount = 0;
			if( bufferPut( receivedByte, &cirBuffer ) == FALSE )	//buffer full?
			{
				rxFinished = TRUE;	//set flag, main needs to reset
				T0IE       = 0;		//disable TMR0 interrupt
			}
		}
		break;
		
		default:					//error
		break;
	}
	T0IF = 0;
	INTF = 0;
}
