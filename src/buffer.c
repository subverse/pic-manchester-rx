/////////////////////////////////////////////////////////
//
//Name:			buffer	
//
//Author:		David McKelvie daveatsubversedotcodotnz
//
//Description:	Circular buffer
//
//Target:		
//
//Version:		
//
//Functions:	bufferInitialise()
//				bufferPut()
//				bufferGet()
//
//Usage:		Declare a variable of type "struct queue",
//				it's your resposibility to think about
//				memory allocation, remember it's going to
//				take up BUFSIZ space.
//				call bufferInitialise() with the address
//				of or pointer to the struct queue.
//				then call:
//					bufferPut()
//					bufferGet()
//				which return TRUE if successfull or
//				FALSE on failure.	
//
/////////////////////////////////////////////////////////
#include<pic.h>
#include"mypic.h"
#include"buffer.h"

//////////////////////////////////////////////////////////
//
//Name:			bufferInitialise
//
//////////////////////////////////////////////////////////
char bufferInitialise( struct queue *ptrBuffer )
{
	ptrBuffer -> head = 0;			//set indexes
	ptrBuffer -> tail = 0;
	return TRUE;
}

//////////////////////////////////////////////////////////
//
//Name:			bufferGet
//
//Description:	takes one character from buffer, placing
//				it at the location pointed to by ptrChar
//
//////////////////////////////////////////////////////////
char bufferGet( char *ptrChar, struct queue *ptrBuffer )
{
	if( ptrBuffer -> head == ptrBuffer -> tail )			//is buffer empty
		return FALSE;
	*ptrChar = ptrBuffer -> buffer[ ptrBuffer -> head ];	//get char
	ptrBuffer -> head ++;									//increment index to buffer head
	if( ptrBuffer -> head == BUFSIZ )						//circulate buffer
		ptrBuffer -> head = 0;
	return TRUE;
}

//////////////////////////////////////////////////////////
//
//Name:			bufferPut
//
//Description:	places Char in buffer
//
//////////////////////////////////////////////////////////
char bufferPut( char Char, struct queue *ptrBuffer )
{
	char tmp;
	tmp = ( ptrBuffer -> tail + 1 ) % BUFSIZ;				//is buffer full
	if( tmp == ptrBuffer -> head )
		return FALSE;
	ptrBuffer -> buffer[ ptrBuffer -> tail ] = Char;		//put char
	if( ++ ptrBuffer -> tail == BUFSIZ )					//circulate  buffer
		ptrBuffer -> tail = 0;
	return TRUE;
}
