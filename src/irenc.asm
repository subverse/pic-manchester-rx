;
;Name:			irenc.asm
;
;Author:		David McKelvie daveatsubversedotcodotnz
;
;Version:		1.0 03/09/2003
;
;Description:	38/40 kHz Infrared transmitter encoder,
;				Encodes signal presented to input pin
;				as 38/40kHz rectangular wave on output pin.
;
;				Encoded signal is either 38 or 40 kHz, depending
;				on the state of the FSEL pin.
;				
;				FSEL = 0    38 kHz
;				FSEL = 1    40 kHz
;
;				DATA_IN/GP0 is an active low input
;
;               ----       ----       ----
;              |10us|16us/|    |     |    |
;              |    |15us |    |     |    |
;            --      -----      -----      -----
;
;                   ----_----
;              VDD||         ||VSS
;                  | 12C508A/|
;                 || 12C509A ||DATA_IN/GP0
;                  |         |
;                 ||         ||DATA_OUT/GP1
;                  |         |
;                 ||         ||FSEL/GP2
;                   ---------
;
;
;
;
;     DATA OUT/GP0--------- 
;                          |
;                         | | R1
;                         | |
;                         | |
;                          |
;                         --- D1 RED LED
;                         \ /
;                         --- 
;                          |
;                         --- D2 IR LED
;                         \ /
;                         ---
;                          |
;                          |
;            ------------------- 0V
;
;  

input	EQU		0
output	EQU		1
fsel	EQU		2

	LIST P=12C508A           
	INCLUDE <p12c508a.inc>
    
;	LIST P=12C509A
;	INCLUDE <p12c509a.inc>

	ERRORLEVEL -224       
    __CONFIG _IntRC_OSC & _WDT_OFF & _CP_OFF & _MCLRE_OFF   

	CBLOCK	0x07
	counter
	ENDC

	ORG	0
main:
	movwf	OSCCAL			;save RC oscillator calibration
	movlw	0x3D			;GP0 input, GP1 output
	tris	GPIO			;GP2, GP3, GP4, GP5 input
	movlw	0xBF			;enable weak pull-ups
	option					;disable wake-up on pin change
							;timer, don't care
							;prescaler, don't care
loop:
	btfsc	GPIO, input		;test input, active low
	goto	loop			;wait for input
pulse:
	bsf		GPIO, output	;activate output
	nop						;keep activated for 10us
	nop						;Boring way to
	nop						;waste some time,
	nop						;it may not look fancy
	nop						;but it does the trick
	nop						;and it makes it easy
	nop						;to change the width
	nop						;of the pulse.
	nop
	bcf 	GPIO, output	;deactivate output
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	btfsc	GPIO, fsel		;adjust frequency
	goto	loop			;this takes 3 or 4
	goto	loop			;cycles depending on
							;state of fsel pin
	end


