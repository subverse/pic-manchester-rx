#ifndef __MYPIC_H__
#define __MYPIC_H__

#ifndef	FALSE
#define FALSE		0
#define TRUE		!FALSE
#endif

#ifndef	NULL
#define	NULL		'\0'
#endif

#ifndef	__DELAY_
#define	__DELAY_
void delay( unsigned int );
#endif

#endif

//Fuses common to PIC12F6XX and PIC16F62X
//
//Brown out detection enable
//BOREN		brown out reset enabled
//BORDIS	brown out reset disabled
//
//Watchdog timer enable
//WDTEN		watchdog timer enabled 
//WDTDIS	watchdog timer disabled
//
//Power up timer enable
//PWRTDIS	power up timer disabled
//PWRTEN	power up timer enabled
//
//Memory clear enable
//MCLREN	memory clear function enabled
//MCLRDIS	memory clear function disabled

//PIC16F62X specific fuses
//
//low voltage programming
//LVPEN		low voltage programming enable
//LVPDIS	low voltage programming disabled
//
//data protection
//DP		protect the data code
//
//code protection
//UNPROTECT	do not protect the code
//PROTECT	protect the program code
//Code protection for 2K program memory	
//#ifdef _16F628		
//#define PROTECT75	0x17FF	/* Protects from 0200h-07ffh	*/
//#define PROTECT50	0x2BFF	/* Protects from 0400h-07ffh	*/
//#else	/* Code protection for 1K program memory	*/
//#define PROTECT50	0x17FF	/* Protects from 0200h-03ffh	*/
//#endif 
//
//osc configurations
//ER		external resistor, CLKOUT on CLKOUT pin, R on CLKIN
//ERIO		external resistor, IO fn on CLKOUT pin, R on CLKIN
//INT		internal resistor/capacitor, CLKOUT on CLKOUT pin, IO on CLKIN
//INTIO		internal resistor/capacitor, IO fn on CLKOUT pin, IO on CLKIN
//EC		external clock in, IO fn on CLKOUT pin, CLKIN on CLKIN
//HS		high speed crystal
//XT		crystal/resonator
//LP		low power crystal

//PIC12F6XX specific fuses
//
//Protection of data block
//CPD		data protect on
//UNPROTECT	data protect off
//
//Protection of program code
//PROTECT	code protection on
//UNPROTECT	code protection off
//
//Oscillator configurations
//RCCLK		GP4 = clock out signal/GP5 = RC osc
//RCIO		GP4 = IO/GP5 = RC osc
//INTCLK	internal osc/GP4 = clock out signal/GP5 = IO
//INTIO		internal osc/GP4 = IO//GP5 = IO
//EC		external clock
//HS		high speed crystal/resonator
//XT		crystal/resonator
//LP		low power crystal/resonator

