#ifndef __PICIR_H__
#define __PICIR_H__

//
//	TMR0 Machine States
//
#define	RESET				1
#define	IDLE				2
#define	SAMPLE				3
#define	DONE				4
#define DO_NOTHING			5
//
//	EDGE Machine States
//
#define	START_BIT_1			2
#define	START_BIT_2			3
#define	CENTER_EDGE			4

#define	IDLE_LINE_TIME		0
#define	START_BIT_TIMEOUT	31
#define	IDLE_LINE			0

#define	INPUT				RB0
#define OUTPUT				RA0

void setup( void );
void interrupt taskManager( void );
void tmr0Machine( void );
void edgeMachine( void );

#endif
