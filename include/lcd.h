//****************************************************
//*		PORTING TO PIC16F628!!!
//****************************************************
//////////////////////////////////////////////////////
//
//	LCD DISPLAY DRIVER
//
//Author:	David McKelvie	
//
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
//
//	LCD DEFINES
//
//////////////////////////////////////////////////////
#ifndef __LCD_H__
#define __LCD_H__
//////////////////////////////////////////////////////
//
//	DATA, CONTROL AND CLOCK DEFINES
//
//////////////////////////////////////////////////////
#define	CONTROL					RB2
#define RS_INSTRUCTION			0
#define	RS_DATA					1
#define RS						RB2
#define E_ENABLE				1
#define E						RB3
#define E_DISABLE				0
#define DATA_LINES				PORTB
#define	LINE_ONE				0x80
#define	LINE_TWO				0xC0
#define	LCD_LINE_ONE			0x80
#define	LCD_LINE_TWO			0xC0
#define LCD_ROWS				2
#define	LCD_COLUMNS				8
//////////////////////////////////////////////////////
//
//	LCD COMMANDS USED WITH LCDIOCTL
//
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
//
//	CLEAR DISPLAY
//
//////////////////////////////////////////////////////
#define LCD_CLEAR_DISPLAY		0x01

//////////////////////////////////////////////////////
//
//	RETURN HOME
//
//////////////////////////////////////////////////////
#define	LCD_RETURN_HOME			0x02

//////////////////////////////////////////////////////
//
//	ENTRY MODE SET
//
//////////////////////////////////////////////////////
#define	LCD_DECREMENT_DD_RAM	0x04
#define	LCD_INCREMENT_DD_RAM	0x06
#define	LCD_NO_SHIFT			0x04
#define	LCD_SHIFT				0x05

//////////////////////////////////////////////////////
//
//	DISPLAY CONTROL COMMANDS
//
//////////////////////////////////////////////////////
#define LCD_DISPLAY_ON			0x0C
#define LCD_DISPLAY_OFF			0x08
#define	LCD_CURSOR_ON			0x0A
#define LCD_CURSOR_OFF			0x08
#define LCD_BLINK_ON			0x09
#define LCD_BLINK_OFF			0x08

//////////////////////////////////////////////////////
//
//	CURSOR OR DISPLAY SHIFT
//
//////////////////////////////////////////////////////
#define	LCD_SHIFT_CURSOR_LEFT	0x10
#define	LCD_SHIFT_CURSOR_RIGHT	0x14
#define	LCD_SHIFT_DISPLAY_LEFT	0x18
#define LCD_SHIFT_DISPLAY_RIGHT	0x1C

//////////////////////////////////////////////////////
//
//	FUNCTION SET
//
//////////////////////////////////////////////////////
#define	LCD_8_BIT				0x30
#define	LCD_4_BIT				0x20
#define	LCD_2_LINE				0x28
#define	LCD_1_LINE				0x20
#define	LCD_5_10_DOTS			0x24
#define	LCD_5_7_DOTS			0x20

//////////////////////////////////////////////////////
//
//	SET CG RAM ADDRESS
//
//	Logical OR with address
//
//////////////////////////////////////////////////////
#define LCD_SET_CG_RAM			0x40

#define LCD_CG_RAM_ZERO			0x00
#define LCD_CG_RAM_ONE			0x08
#define LCD_CG_RAM_TWO			0x10
#define LCD_CG_RAM_THREE		0x18
#define LCD_CG_RAM_FOUR			0x20
#define LCD_CG_RAM_FIVE			0x28
#define LCD_CG_RAM_SIX			0x30
#define	LCD_CG_RAM_SEVEN		0x38

//////////////////////////////////////////////////////
//
//	SET DD RAM ADDRESS
//
//	Logical OR with address
//
//////////////////////////////////////////////////////
#define	LCD_SET_DD_RAM			0x80

//////////////////////////////////////////////////////
//
//	LCD PROTOTYPES
//
//////////////////////////////////////////////////////
void lcdWrite( char );
void lcdIoctl( char );
void lcdOpen( void );
void lcdPrint( const char *, char );
void lcdSetCGRam( char, char * );
char lcdScroll( const char * );
void lcdBarInitialise( void );
char lcdSetBargraph( char, char * );

//////////////////////////////////////////////////////
//
//	CG RAM BIT PATTERNS
//	
//////////////////////////////////////////////////////
#define BAR_ONE_PATTERN			0x10
#define BAR_TWO_PATTERN			0x18
#define BAR_THREE_PATTERN		0x1C
#define BAR_FOUR_PATTERN		0x1E
#define BAR_FIVE_PATTERN		0x1F

//unsigned char leftArrow[]		= { 0x01, 0x03, 0x07, 0x0F, 0x07, 0x03, 0x01, 0x00 };
//unsigned char checker[]		= { 0x15, 0x0A, 0x15, 0x0A, 0x15, 0x0A, 0x15, 0x00 };

#define BAR_ONE					1
#define	BAR_TWO					2
#define BAR_THREE				3
#define	BAR_FOUR				4
#define BAR_FIVE				5
#define BARSIZ					8

#define SPACE					0x20

#endif
